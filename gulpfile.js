const gulp = require('gulp'),
  shell = require('gulp-shell'),
  argv = require('yargs').argv,
  path = require('path'),
  wantu = require('./upload/plugins/gulp-wantu.js');
  moment = require('moment');

const ak = argv.AK || '****',
  sk = argv.SK  || '****',
  namespace = argv.NAMESPACE || 'test';

const domain = `${namespace}.image.alimmdn.com`,
  dir = `vue-admin-front/${moment().format('YYYYMMDDHHmmss')}`;

let publicPath = `//${domain}/${dir}`;



gulp.task('build',shell.task(`npm run build --PUBLIC_PATH=${publicPath}/`));

gulp.task('wantu', () => {
  gulp.src(path.resolve(__dirname, './dist/**')).pipe(wantu({
    AK: ak,
    SK: sk,
    namespace: namespace
  }, {
    dir: `/${dir}`
  }))
});


gulp.task('default',['build'],()=>{
  gulp.start('wantu')
});