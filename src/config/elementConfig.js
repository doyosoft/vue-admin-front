export default {
  size: process.env.UI_SIZE === 'mini' ? 'mini' : 'small'
};
