import debounce from 'lodash/debounce'

//#region resize
const resizeEvent = document.createEvent("HTMLEvents");
resizeEvent.initEvent("resize", false, true);
//#endregion

export default {
  install(Vue, options) {
    let $trigger = Vue.prototype.$ccTrigger = {};
    Object.assign($trigger, {
      resize: debounce(() => {
        window.dispatchEvent(resizeEvent);
      }, 100)
    })
  }
}
