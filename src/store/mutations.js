export default {
  setUserInfo(state, value) {
    state.userInfo = value
  },
  setRouteItem(state, value) {
    state.routeItem = value
  },

  setRouteDone(state, value) {
    state.routeDone = true
  },

  setRouteUrlMeta(state, value) {
    state.routeUrlMeta = value
  },

  isIframeOuter(state, value) {
    state.isIframeOuter = value;
  },

  setIframeOuterUrl(state, value) {
    state.iframeOuterUrl = value;
  },

  setAppLayoutMenuCollapse(state, value) {
    state.appLayoutMenuCollapse = value;
  }
}
