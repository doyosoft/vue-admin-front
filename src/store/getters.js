export default {
  isIframeEmbed() {
    return self != top;
  }
}
