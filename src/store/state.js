export default {
  /**项目 */
  project: "vue-admin-front",
  /**项目中文名, 页头显示用 */
  projectName: "vue-admin",
  /**用户信息 */
  userInfo: {},
  /**路由 */
  routeItem: [],
  /**路由加载完成 */
  routeDone: false,
  /**内嵌iframe路由单独存放 */
  routeUrlMeta: {},
  /**当前路由是否是内嵌iframe */
  isIframeOuter: false,
  /**当前路由内嵌iframe Url */
  iframeOuterUrl: "",
  /**菜单栏是否收起 , 仅显示图标*/
  appLayoutMenuCollapse: false
}
