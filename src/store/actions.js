import axios from "axios"
import {
  auth
} from "@/api"

export default {

  async setUserInfoAsync({
    commit
  }) {
    let userInfo = {},
      result = null;
    try {
      result = await axios.get(auth.user);
      if (result && result.data) {
        userInfo.name = result.data.username || "某某";
        userInfo.avatar = result.data.avatar;
      }
    } catch (ex) {
      userInfo.name = "某某"
      console.error(ex)
    }
    commit("setUserInfo", userInfo);
    return result;
  },
  /**获取菜单/路由信息 */
  async setRouteItemAsync({
    commit
  }) {
    let menu = [];
    try {
      let result = await axios.get(auth.menu);
      if (result && result.data) {
        menu = result.data;
      }
    } catch (ex) {
      console.error(ex);
    }
    commit("setRouteItem", menu);
    commit("setRouteDone", true);
  },
}
