import vue from "vue"
import Vuex from "vuex"

import state from "./state"
import mutations from "./mutations"
import actions from "./actions"
import getters from "./getters"

export default function creatStore() {
  vue.use(Vuex);
  return new Vuex.Store({
    state,
    mutations,
    actions,
    getters
  })
};