import KeySymbol from "./keySymbol"

export default (rowKey, newRowData, data, {
  treeKey
}) => {
  let row = data.find(item => {
    return item[treeKey] === rowKey;
  });
  console.log(rowKey, row, newRowData);
  if (row) {
    let {
      [treeKey]: rowKey, [KeySymbol.rowIdSymbol]: rowId, [KeySymbol.levelSymbol]: level, [KeySymbol.hasChildrenSymbol]: hasChildren, [KeySymbol.parentIdSymbol]: parentId, [KeySymbol.expandedSymbol]: expanded, [KeySymbol.showSymbol]: show,
      ...rest
    } = newRowData;
    console.log("rest", rest);
    Object.assign(row, rest);
  }
}
