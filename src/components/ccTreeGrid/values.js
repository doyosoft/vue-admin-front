import KeySymbol from "./keySymbol"

let values = [];

/**
 * 处理同一parentId的数据
 * 
 * @param {*} data 
 * @param {*} values 
 * @param {*} currentParentId 
 */
function levelData(data, values, currentParentId) {

  let matchedData = [],
    unmatchdata = [];

  data.forEach(item => {
    if (item[KeySymbol.parentIdSymbol] == currentParentId) {
      matchedData.push(item);
    } else {
      unmatchdata.push(item);
    }
  });
  
  data.splice(0, data.length, ...unmatchdata);


  matchedData.forEach(item => {
    let {
      [KeySymbol.rowIdSymbol]: rowId, [KeySymbol.levelSymbol]: level, [KeySymbol.hasChildrenSymbol]: hasChildren, [KeySymbol.parentIdSymbol]: parentId, [KeySymbol.expandedSymbol]: expanded, [KeySymbol.showSymbol]: show,
      ...rest
    } = item;
    let value = {
      ...rest
    };
    values.push(value)
    if (hasChildren) {
      value.children = [];
      levelData(data, value.children, rowId)
    }
  });
}

export default (data) => {
  const meataData = [...data];
  values = [];
  levelData(meataData, values, undefined);
  return values;
}
