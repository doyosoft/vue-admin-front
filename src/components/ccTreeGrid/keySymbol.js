export default {
  rowIdSymbol: Symbol('rowId'),
  levelSymbol: Symbol('level'),
  hasChildrenSymbol: Symbol('hasChildren'),
  parentIdSymbol: Symbol('parentId'),
  expandedSymbol: Symbol('expanded'),
  showSymbol: Symbol('show'),
}
