import KeySymbol from './keySymbol';

/**
 * 获取 parent的key
 * 
 * @param {*} rowId 
 * @param {*} param1 
 */
function parentKey(rowId, data, {
  treeKey
}) {
  let parent = data.find(item => {
    return item[KeySymbol.rowIdSymbol] === rowId;
  });
  if (parent) {
    parentKeys.push(parent[treeKey]);
    if (parent[KeySymbol.parentIdSymbol]) {
      parentKey(parent[KeySymbol.parentIdSymbol], data, {
        treeKey
      });
    }
  }
}

let parentKeys = [];

export default (keys, data, {
  treeKey
}) => {
  parentKeys = [];
  keys.forEach(item => {
    let current = data.find(dataItem => {
      return dataItem[treeKey] === item;
    });
    if (current && current[KeySymbol.parentIdSymbol]) {
      parentKey(current[KeySymbol.parentIdSymbol], data, {
        treeKey
      });
    }
  });

  return [...keys, ...parentKeys]
}
