import KeySymbol from "./keySymbol";
import {
  uniqueId
} from "lodash"

/**
 * 父节点可以展开
 * 
 * @param {*} rowKey 
 * @param {*} data 
 * @param {*} param2 
 */
function parentExpandableHandler(rowKey, data, {
  treeKey
}) {
  let row = data.find(item => {
    return item[treeKey] === rowKey;
  });
  if (row) {
    row[KeySymbol.hasChildrenSymbol] = true;
    row[KeySymbol.expandedSymbol] = row[KeySymbol.expandedSymbol] === false ? false : true
  }
}

/**
 * 计算在Data中的位置
 * 
 * @param {*} parentRowKey 
 * @param {number} position 
 * @param {*} data 
 */
function calDataPosition(parentRowKey, position, data, {
  treeKey
}) {
  //#region position可能为空, 非数值, 小于零, 重置position为合法范围
  if (Number.isInteger(position)) {
    if (position < 0) {
      position = 0;
    }
  } else {
    if (!parentRowKey || parentRowKey <= 0) {
      position = data.filter(item => {
        return item[KeySymbol.parentIdSymbol] === undefined;
      }).length;
    } else {
      const parent = data.find(item => {
        return item[treeKey] === parentRowKey;
      });
      position = data.filter(item => {
        return item[KeySymbol.parentIdSymbol] === parent[KeySymbol.rowIdSymbol]
      }).length;
    }
  }
  //#endregion


  if (!parentRowKey || parentRowKey <= 0) {
    let siblings = data.filter(item => {
      return item[KeySymbol.parentIdSymbol] === undefined;
    });

    if (position >= siblings.length) {
      return data.length;
    } else {
      return data.indexOf(siblings[position]);
    }
  } else {
    let parent = data.find(item => {
      return item[treeKey] === parentRowKey;
    });
    if (parent) {
      // 兄弟节点
      let siblings = data.filter(item => {
        return item[KeySymbol.parentIdSymbol] === parent[KeySymbol.rowIdSymbol]
      });
      // 新添加的位置超出兄弟节点范围, 取父节点下一个兄弟节点位置
      if (position >= siblings.length) {
        let index = getEdgeIndex(parent, data);
        return index;
      } else {
        return data.indexOf(siblings[position]);
      }
    }
  }

  return false;
}

/**
 * 获取边缘索引
 * @param {*} parent 
 * @param {*} data 
 */
function getEdgeIndex(parent, data) {
  let parentSiblings = data.filter(item => {
    return item[KeySymbol.parentIdSymbol] === parent[KeySymbol.parentIdSymbol];
  });

  const parentIndex = parentSiblings.indexOf(parent);

  //parent已经在其兄弟节点的最末位, 继续向上查询
  if (parentIndex === parentSiblings.length - 1) {
    const parentId = parent[KeySymbol.parentIdSymbol];
    parent = data.find(item => {
      return item[KeySymbol.rowIdSymbol] === parentId;
    });
    if (parent) {
      return getEdgeIndex(parent, data);
    } else {
      // 父节点的父节点为空则到达根节点
      return data.length;
    }
  } else {
    return data.indexOf(parentSiblings[parentIndex + 1])
  }
}
/**
 * 插入行
 * 
 * @param {*} parentRowKey 
 * @param {*} row 
 * @param {Number} position 插入位置
 * @param {*} data 
 * @param {*} param3 
 */
function insertData(parentRowKey, row, position, data, {
  treeKey
}) {
  // 如果未指定父节点, 则放到根节点
  if (!parentRowKey || parentRowKey <= 0) {
    let insertPosition = calDataPosition(parentRowKey, position, data, {
      treeKey
    });
    data.splice(insertPosition, 0, {
      [KeySymbol.rowIdSymbol]: uniqueId("row_"),
      [KeySymbol.levelSymbol]: 0,
      [KeySymbol.showSymbol]: true,
      ...row
    });
  } else {
    let parent = data.find(item => {
      return item[treeKey] === parentRowKey;
    });
    if (parent) {
      let insertPosition = calDataPosition(parentRowKey, position, data, {
        treeKey
      });
      if (insertPosition) {
        data.splice(insertPosition, 0, {
          [KeySymbol.rowIdSymbol]: uniqueId("row_"),
          [KeySymbol.levelSymbol]: parent[KeySymbol.levelSymbol] + 1,
          [KeySymbol.parentIdSymbol]: parent[KeySymbol.rowIdSymbol],
          [KeySymbol.showSymbol]: parent[KeySymbol.expandedSymbol],
          ...row
        });
      }
    }
  }

}

export default (parentRowKey, row, position, data, {
  treeKey
}) => {
  parentExpandableHandler(parentRowKey, data, {
    treeKey
  });
  insertData(parentRowKey, row, position, data, {
    treeKey
  });
}
