// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'

// #region 样式
import 'element-ui/lib/theme-chalk/index.css'

import './assets/css/normalize.css'
import './assets/css/common.css'
// #endregion

//#region nprogress
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
//#endregion

import axios from 'axios'
import VueAxios from 'vue-axios'
import VueCookie from 'vue-cookie'

import './config/axiosConfig'
import elementConfig from './config/elementConfig'
import createRouter from './router';
import createStore from './store';

if (process.env.UI_SIZE === 'mini') {
  require('../theme/build/cc-custom-theme/custom/index.css');
}

Vue.use(ElementUI, elementConfig);

Vue.use(VueCookie)
Vue.use(VueAxios, axios)

import triggerEvent from '@/plugins/triggerEvent.js'
Vue.use(triggerEvent);

Vue.config.productionTip = false

const store = createStore();
const router = createRouter({
  store,
  NProgress
});

/* eslint-disable no-new */
new Vue({
  //el: '#app',
  router,
  store,
  render: h => h(App)
}).$mount('#app');

if (false && process.env.NODE_ENV === "production") {
  require('./common/forbidDebugger.js');
}
