﻿/**
 * 需要通过权限控制的路由
 */
import main from '../views/main'

export default [{
    path: '/example',
    meta: {
      name: 'example'
    },
    component: main,
    children: [{
      name: 'example',
      path: '/',
      component: resolve => require(['../views/example'], resolve)
    }]
  }, {
    path: '/example-pdfjs',
    meta: {
      name: 'example-pdfjs'
    },
    component: main,
    children: [{
      name: 'example-pdfjs',
      path: '/',
      component: resolve => require(['../views/example-pdfjs'], resolve)
    }]
  },
  {
    path: '/example-treegrid',
    meta: {
      name: 'example-treegrid'
    },
    component: main,
    children: [{
      name: 'example-treegrid',
      path: '/',
      component: () =>
        import ('../views/example-treegrid')
    }]
  }
]
