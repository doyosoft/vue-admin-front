﻿/**
 * 不需要权限控制的路由
 */
import main from '../views/main'

export default [{
  path: '/(about)?',
  meta: {
    name: 'about'
  },
  component: main,
  children: [{
    name: 'about',
    path: '/',
    component: resolve => require(['../views/about'], resolve)
  }]
}];

export const notFoundRouter = {
  path: `*`,
  meta: {
    name: 'NoFound' // 通过菜单里的name与该meta信息匹配确定是否加载该路由
  },
  component: main, // 页面布局
  children: [{
    name: 'NoFound',
    path: '/',
    component: resolve => require(['../views/notFound'], resolve) // 对应的页面模块
  }]
};
