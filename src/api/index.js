const basePath = process.env.BASE_PATH || "";

export const auth = {
  menu: '/home/getmenu',
  user: '/home/getuser',
  logout: '/home/logout'
}

export const api = {
  log: `${basePath}/log`
}
