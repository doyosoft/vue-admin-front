 function forbidDebugger() {
   setTimeout(() => {
     ((function () {})['constructor']('debugger'))();
     forbidDebugger();
   }, 250);
 }
 forbidDebugger();
