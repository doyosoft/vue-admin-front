# 前端架构

## 目录机构

``` test
|-- build   # 构建时具体事项, 核心
    |-- build.js # 构建时所作具体事项, 核心在于webpack打包
    |-- check-version.js
    |-- dev-client.js
    |-- dev-server.js # 开发时的具体事项, 比如启动服务, 热更新等等 
    |-- utils.js
    |-- vue-loader.conf.js
    |-- webpack.base.conf.js # webpack 基础配置
    |-- webpack.dev.conf.js # webpack 开发时配置
    |-- webpack.prod.conf.js # webpack 打包配置
|-- config   # 基本配置, 比如开发构建时需要的基本参数 
    |-- index.js  # build 构建时基本参数; dev 开发时基本参数
    |-- dev.env.js
    |-- prod.env.js
|-- mock # 开发时模拟数据, 菜单, 用户信息, 登出等
|-- src # 业务逻辑源代码
    |-- api
    |-- assets
    |-- components # 通用组件
    |-- config # 配置
    |-- router # 路由,  具体页面直接指向到具体模块文件夹
        |-- index.js
    |-- views # 模块文件夹
        |-- about # about 模块
            |-- index.vue # hello 模块入口, 整合子模块
            |-- index.less # 样式文件
        |--main.vue # 总体结构
    |-- App.vue # 真正入口
    |-- main.js # 依赖包, 路由, 页面入口
|-- static
|-- index.html # 前端入口文件, 构建后的文件会自动注入
|-- package.json # npm的配置文件
```

## Run 起来

```base
# 安装依赖包
> npm install

# 开发
> npm run dev

# 打包
> npm run build
> npm run build --report
```

> 在`npm run dev` 或 `npm run build`时添加vars `--BASE_PATH=path` 会向`process.env`对象中添加属性`BASE_PATH=path`, 这可以用于指定项目接口等路径；`--UI_SIZE="mini"`会使用`mini`主题

## 路由和菜单

路由文件放置于`~/src/router`文件夹，`index.js`是路由的入口文件，`authorityRouter.js`为需要通过权限控制的路由信息，`constantRouter.js`不受权限控制影响; 路由也可以控制第三方页面的嵌入。具体路由配置请参考`constantRouter.js`。

权限控制主要由菜单接口返回，控制菜单的显示与否及路由是否加载。具体菜单配置请参考`~/mock/home/menu`。
