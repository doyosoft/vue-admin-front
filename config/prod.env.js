module.exports = {
  NODE_ENV: '"production"',
  BASE_PATH: `"${process.env.npm_config_BASE_PATH || ''}"`,
  UI_SIZE: `"${process.env.npm_config_UI_SIZE || ''}"`
}
