
## 知识点

整个前端是使用vue进行开发, 所以在使用当前框架时需要储备些相关基础知识

1. [es2015/es2017](http://es6.ruanyifeng.com/)基础知识, 配合[axios](https://www.npmjs.com/package/axios)会异常的方便
2. [vue](https://cn.vuejs.org/)的基础知识 绑定+指令+模板等等
3. [element-ui](http://element.eleme.io/#/zh-CN/component/installation)基础知识, 其实知道了vue的组件概念, 这块使用起来会非常的简单
4. less基础知识  好用的css预编译, 用起来比css方便很多
5. 常用组件axios, qs, moment, lodash等等

## 技术栈

### [Emmet](https://docs.emmet.io/)

emmet是html,css的辅助工具, 可以很方便的帮助我们生成html,css; 但我们需要重复写一类html, 我们可以使用emmet很方便的生成这些重复的html 

1. 表格 el-table[:data="" :border="true"]>(el-table-column[align="center" label="" width=""]>template[slot-scope="scope"]{{{scope.row}}})*4
2. form表单 el-form[label-width=""]>el-form-item[label=""]*8
3. 

### [Element](http://element.eleme.io/)

### tooltip 组件

tooltip 内不支持 disabled form 元素, 请在 disabled form 元素外层添加一层包裹元素。

```html
<el-tooltip placement="top">
  <div slot="content">德玛西亚</div>
  <span>
    <el-button type="primary" size="small" :disabled="true">啦啦啦</el-button>
  </span>
</el-tooltip>
```

### [axios](https://www.npmjs.com/package/axios)

#### .all

简单的并发请求, 配合async/await, 方法比.net并发简单了很多

```javascript
let results = await axios.all([request1, request2, request3]);//三个请求同时发起, 并等待三个请求全部结束
results.length//results 是一个请求响应的数组, 每一个元素代表一个请求响应结果
results[0].data//可以像普通请求一样处理响应结果
```

## 开发工具

### Visula Studio Code

#### 扩展插件
1. **Vetur**,  vue 开发必备
2. **TODO Hightlight**, TODO:高亮
3. **vscode-icons**, 给你的文件换个图标 
4. **Visual Studio Keymap**, 习惯了vs快捷键的小伙伴可以试试
5. **Bracket Pair Colorizer**, 给你的大括号加个颜色
6. **Code Spell Checker**, 单词是不是又拼写错了, 试试这个插件
